FROM node
MAINTAINER Nuxt.tBug
ENV NODE_ENV=production
RUN mkdir -p /app
COPY . /app
WORKDIR /app
EXPOSE 80
RUN echo 'build tibug image successful!!'
#此为cnpm淘宝镜像
#RUN npm config set registry https://registry.npm.taobao.org
RUN npm install
RUN npm run build
CMD ["npm", "start"]